from ase.calculators.vdwcorrection import vdWTkatchenko09prl
from gpaw.analyse.hirshfeld import HirshfeldPartitioning
from gpaw.analyse.vdwradii import vdWradii
from ase import Atoms
from gpaw import GPAW
from ase.io import read
from ase.optimize import BFGSLineSearch
from ase.calculators.emt import EMT
from time import time

a = 0.18
l = a*104
energies = ['0.00', '0.05', '0.11', '0.17', '0.21', '0.23', '0.26', '0.36']
dat = open('2energies.dat', 'w')
dat.write('# delta-energy_vivianne energy_gridTS09 delta_e cell_size time\n')

for ene in energies:
    t1 = time()
    e = 1
    mod = read('../../CO2_on_Cu13_structures/CO2_on_Cu13_'+ene+'/vesta.xyz')
    mod.set_cell([l,l,l])
    mod.center()
    c = GPAW(h = a,
                xc = 'PBE',
                spinpol = True,
                symmetry = 'off')
    calc = vdWTkatchenko09prl(HirshfeldPartitioning(c), vdWradii(mod.get_chemical_symbols(), 'PBE'))
    mod.calc = calc
    dyn = BFGSLineSearch(atoms=mod, trajectory = '2energies{}.traj'.format(ene))
    try:
        dyn.run(fmax = 0.05)
    except:
        e = 0
    if e!=0:
        e = mod.get_potential_energy()
    if ene == '0.00':
        min = e
    c = mod.cell
    t = time()-t1
    dat.write(str(ene)+'\t'+str(e)+'\t'+str(e-min)+'\t'+str(c.lengths())+'t'+str(t)+'\n')
dat.close()

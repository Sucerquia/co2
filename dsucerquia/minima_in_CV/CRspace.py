from ase.io import read
from ase.calculators.emt import EMT
from ase.calculators.plumed import Plumed
from ase.visualize import view
from ase import Atoms

i = ["c: COORDINATION GROUPA=1-5 GROUPB=1-5 R_0=2.8",
     "r: GYRATION ATOMS=1-5",
     "PRINT ARG=c,r STRIDE=1 FILE=COLVAR"]

Cu13 = Atoms('Cu13')
calc = Plumed(EMT(),i, 1, Cu13, log = 'plumed_bipi.log')
Cu13.calculator = calc
calc.atoms = Cu13
energies = ['0.00', '0.05', '0.11', '0.17', '0.21', '0.23', '0.26', '0.36']
for ener in energies:
    mod = read('../../CO2_on_Cu13_structures/CO2_on_Cu13_'+ener+'/vesta.xyz')
    del mod[[0,1,2]]
    Cu13.set_positions(mod.get_positions())
    pos = mod.get_positions()
    calc.compute_energy_and_forces(pos,energies.index(ener))

mod = read('../../CO2_on_Cu13_structures/Cu13/vesta.xyz')
#del mod[[0,1,2]]
Cu13.set_positions(mod.get_positions())
pos = mod.get_positions()
calc.compute_energy_and_forces(pos,energies.index(ener))

calc.close()

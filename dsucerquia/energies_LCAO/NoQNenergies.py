from ase import Atoms
from gpaw import GPAW
from ase.io import read
from ase.optimize import BFGSLineSearch
from ase.calculators.emt import EMT

energies = ['0.00', '0.05', '0.11', '0.17', '0.21', '0.23', '0.26', '0.36']
dat = open('NoQNenergies.dat', 'w')
dat.write('# delta-energy_vivianne energy_LCAO')
for ene in energies:
    e = 1
    mod = read('../../CO2_on_Cu13_structures/CO2_on_Cu13_'+ene+'/vesta.xyz')
    mod.center(vacuum = 5.0)
    calc = GPAW(h = 0.2,
                mode = 'lcao',
                basis = 'dzp',
                xc = 'PBE',
                spinpol = True,
                symmetry = 'off')
    mod.calc = calc
    try:
        e = mod.get_potential_energy()
    except:
        e = 0
    dat.write(str(ene)+'\t'+str(e)+'\n')
dat.close()

from ase import Atoms
from gpaw import GPAW
from ase.io import read
from ase.optimize import BFGSLineSearch
from ase.calculators.emt import EMT
from time import time
a = 0.18
l = a*104
energies = ['0.00', '0.05', '0.11', '0.17', '0.21', '0.23', '0.26', '0.36']
dat = open('energies.dat', 'w')
dat.write('# delta-energy_vivianne energy_LCAO delta_LCAO cell_size\n')

for ene in energies:
    t1 = time()
    e = 1
    mod = read('../../CO2_on_Cu13_structures/CO2_on_Cu13_'+ene+'/vesta.xyz')
    mod.set_cell([l, l, l])
    mod.center()
    calc = GPAW(h = a,
                mode = 'lcao',
                basis = 'dzp',
                xc = 'PBE',
                spinpol = True,
                symmetry = 'off')
    mod.calc = calc
    dyn = BFGSLineSearch(atoms=mod, trajectory = 'energies{}.traj'.format(ene))
    try:
        dyn.run(fmax = 0.05)
    except:
        e = 0
    if e!=0:
        e = mod.get_potential_energy()
    if ene == '0.00':
        min = e
    t = time() - t1
    dat.write(str(ene)+'\t'+str(e)+'\t'+str(e-min)+'\t'+str(t)+'\n')
dat.close()
